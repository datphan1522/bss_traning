<?php
namespace Bss\Webservice\Model;
use Bss\Webservice\Api\Data\Item;
use Bss\Webservice\Api\PostManagementInterface;
class PostManagement implements PostManagementInterface
{


    /**
     * {@inheritdoc}
     */

    /**
     * GET for Post api
     * @param string $id
     * @param string $name
     * @param string $class
     * @return string
     */

    public function customGetMethod($id)
    {
        try{
            // Tạo mảng chứa 5 items
            $items = [
                1 => new Item('1', 'Item 1', 'Class A'),
                2 => new Item('2', 'Item 2', 'Class B'),
                3 => new Item('3', 'Item 3', 'Class C'),
                4 => new Item('4', 'Item 4', 'Class D'),
                5 => new Item('5', 'Item 5', 'Class E'),
            ];
            
            if (isset($items[$id])) {
                // Trả về data tương ứng với item ID là $id
                $item = $items[$id];
    
                $response = [
                    [
                        'id' => intval($item->id),
                        'name' => $item->name,
                        'class' => $item->class
                    ]
                ];
            } else {
                // Trả về mảng các đối tượng item
                $response = [];
                foreach ($items as $item) {
                    $response[] = [
                        'id' => intval($item->id),
                        'name' => $item->name,
                        'class' => $item->class
                    ];
                }
            }


        }catch(\Exception $e) {
            $response=['error' => $e->getMessage()];
        }

        return $response;
        
    }
 
    // /**
    //  * {@inheritdoc}
    //  */
    // /**
    //  * GET for Post api
    //  * @param string $storeid
    //  * @param string $name
    //  * @param string $city
    //  * @return string
    //  */

    // public function customPostMethod($storeid,$name,$city)
    // {
    //     try{
    //         $response = [
    //             'storeid' => $storeid,
    //             'name' =>$name,
    //             'city'=>$city
    //         ];
    //     }catch(\Exception $e) {
    //         $response=['error' => $e->getMessage()];
    //     }
    //     return json_encode($response);
    // }
}