<?php
namespace Bss\Webservice\Api\Data;
class Item {
 
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $class;

    /**
     * Item constructor.
     * @param string $id
     * @param string $name
     * @param string $class
     */
    public function __construct($id, $name, $class)
    {
        $this->id = $id;
        $this->name = $name;
        $this->class = $class;
    }

}