<?php
namespace Bss\CustomNewsletter\Block;

use Magento\Framework\View\Element\Template;

class CustomBlock extends Template
{
    protected function _prepareLayout()
    {
        $this->addChild(
            'form.subscribe',
            \Magento\Newsletter\Block\Subscribe::class,
            ['template' => 'Bss_Newsletter::subscribe.phtml']
        );
        $this->addChild(
            'custom_footer_links_block',
            \Magento\Cms\Block\Block::class,
            ['block_id' => 'custom_footer_links_block']
        );
        return parent::_prepareLayout();
    }
}