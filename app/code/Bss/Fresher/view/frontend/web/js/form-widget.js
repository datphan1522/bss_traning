define([
    'jquery',
    'jquery-ui-modules/widget',
    'Magento_Ui/js/modal/modal'
], function ($, modal) {
    'use strict';

    $.widget('mage.formWidget', {
        options: {
            formSelector: '#fresher-form',
            submitSelector: '#fresher-form button[type="submit"]',
            requiredFieldsSelector: '[required]',
        },
        _create: function () {
            var self = this;

            this.element.on('submit', function (e) {
                e.preventDefault();
                if (self.validateForm()) {
                    self.submitForm();
                }else{
                    alert("missing message field!!")
                }
            });
        },

        _validateField: function (selector) {
            var field = $(selector);
            var value = field.val();
        
            if (!value) {
                field.addClass('error');
                return false;
            } else {
                field.removeClass('error');
                return true;
            }
        },
        
        validateForm: function () {
            var self = this;
            var valid = true;

            valid = valid && this._validateField('#name');
            valid = valid && this._validateField('#telephone');
            valid = valid && this._validateField('#dob');
            valid = valid && this._validateField('#message');
            return valid;
        },

        submitForm: function () {
            console.log('Form submitted!');
            var self=this;
            var formData = $(this.options.formSelector).serialize();
            var submitUrl = $(this.options.formSelector).attr('action')
            console.log(formData);

            $.ajax({
                url: submitUrl,
                type: 'POST',
                dataType: 'json',
                data: formData,
                success: function (response) {
                    console.log(response.data);
                    self.showPopups(response.data)
                },
                error: function (response, error) {
                    console.log(error);
                    alert(response.message)
                }
            });
        },

        showPopups: function (data) {
            $('#popup-template').css('display','block');

            $('#popup-template #name').text(data.name)
            $('#popup-template #phone').text(data.telephone)
            $('#popup-template #dob').text(data.dob)
            $('#popup-template #message').text(data.message)

            $('#popup-template #close-btn').on('click', function () {
                $('#popup-template').hide();
                $('#overlay').hide();
            })

            $('#popup-template').show()
            $('#overlay').show();
        }

    });

    return $.mage.formWidget;
});
