<?php
namespace Bss\Fresher\Controller\Fresher;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Requirejs extends Action
{
    protected $resultPageFactory;

    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        return $this->resultPageFactory->create();
    }
}

// // namespace Vendor\Module\Controller;
// class Requirejs implements \Magento\Framework\App\RouterInterface
// {
//    protected $actionFactory;
//    protected $_response;
//    public function __construct(
//        \Magento\Framework\App\ActionFactory $actionFactory,
//        \Magento\Framework\App\ResponseInterface $response
//    ) {
//        $this->actionFactory = $actionFactory;
//        $this->_response = $response;
//    }
//    public function match(\Magento\Framework\App\RequestInterface $request)
//    {
//        $identifier = trim($request->getPathInfo(), '/');
//        if(strpos($identifier, 'abc') !== false) {
//        $request->setModuleName('fresher')-> //module name
//         setControllerName('fresher')-> //controller name
//         setActionName('requirejs')-> //action name
//         setParam('param', 3); //custom parameters
//        } else {
//            return false;
//        }
//        return $this->actionFactory->create(
//            'Magento\Framework\App\Action\Forward',
//            ['request' => $request]
//        );
//    }
// }