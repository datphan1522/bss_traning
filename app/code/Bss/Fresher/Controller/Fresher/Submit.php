<?php
namespace Bss\Fresher\Controller\Fresher;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Submit extends Action
{
    protected $resultJsonFactory;

    public function __construct(Context $context, JsonFactory $resultJsonFactory)
    {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();

        $response = ['error' => false, 'data' => []];

        if (empty($data['name']) || empty($data['telephone']) || empty($data['dob']) || empty($data['message'])) {
            $response['error'] = true;
            $response['message'] = 'Please fill in message fields.';
        } else {
            $response['data']['name'] = $data['name'];
            $response['data']['telephone'] = $data['telephone'];
            $response['data']['dob'] = $data['dob'];
            $response['data']['message'] = $data['message'];
        }

        return $this->resultJsonFactory->create()->setData($response);
    }
}

