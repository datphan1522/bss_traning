<?php
namespace Bss\Newsletter\Block;

class Subscribe extends \Magento\Newsletter\Block\Subscribe
{
    public function setTemplate($template)
    {
        $template = 'Bss_Newsletter::subscribe.phtml';
        return parent::setTemplate($template);
    }
}