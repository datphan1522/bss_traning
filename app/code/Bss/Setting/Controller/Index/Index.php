<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Bss\Setting\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;


class Index implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param RequestInterface $request
     */

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        RequestInterface $request
    ) {
    
        $this->pageFactory = $pageFactory;
        $this->request = $request;
    }

    /**
     * Generates robots.txt data and returns it as result
     *
     * @return Page
     */
    public function execute()
    {
        $firstParam = $this->request->getParam('first_param', null);
        $secondParam = $this->request->getParam('second_param', null);
        $resultPage = $this->pageFactory->create();

        $resultPage->addHandle('setting_index_index');
        return $resultPage;
    }
}