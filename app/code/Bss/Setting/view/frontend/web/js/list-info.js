define([
    'jquery',
    'uiComponent',
    'ko'
], function ($, Component, ko) {
    'use strict';
    return Component.extend({
        defaults: {
        },
        initialize: function () {
            this.customerList = ko.observableArray([]);
            this.id = ko.observable();
            this.name = ko.observable();
            this.class = ko.observable();

            this._super();

        },

        addNewCustomer: function () {
            if (this.id && this.name && this.class) {
                var dataInfo = {
                    id: this.id(),
                    name: this.name(),
                    class: this.class(),
                }
                this.customerList.push(dataInfo);

                this.id("");
                this.name("");
                this.class("");

            }
        },

        resetList: function () {
            this.customerList([]);
        },

        deleteCustomer: function (customer) {
            this.customerList.remove(customer);
        }
    });
}
);